#!/bin/bash

_DATE=$(date "+%a,%d/%b/%Y %R")
_BATTERY="$(cat /sys/class/power_supply/BAT0/capacity)%"

_WIFI=$(iw dev wlan0 info | grep ssid | awk '{ print $2 }')

_MEMORY=$(free -h | grep Mem | awk '{ print $7 }')

if [[ -n $_WIFI ]];
then
	_CONNECTION="$_WIFI"
else
	_CONNECTION="x.x"
fi


_MAINSTRING="$_MEMORY - $_CONNECTION - $_DATE - $_BATTERY"

echo $_MAINSTRING
