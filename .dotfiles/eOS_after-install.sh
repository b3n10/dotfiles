#update eOS
sudo apt update
sudo apt dist-upgrade

#install other programs
#zram for ram compression (only useful for 2G ram), reboot required
sudo apt install zram-config

#htop, vim, vlc, git, chrome
sudo apt install htop
sudo apt install vim
sudo apt install vlc
sudo apt install git
cd /tmp
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get install -f

# vundle.vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

#dot-files
git clone https://github.com/b3n10/dot-files.git
cd dot-files/
mv .dotfiles/ .git/ .gitignore -t ~/

#elementary fixes
cd ~/.dotfiles/eOS-fixes/
sudo mv ath9k.conf /etc/modprobe.d/ath9k.conf
sudo mv audio_powersave.conf /etc/modprobe.d/audio_powersave.conf
sudo mv dirty.conf /etc/sysctl.d/dirty.conf
sudo mv laptop.conf /etc/sysctl.d/laptop.conf
sudo mv pci_powersave.conf /etc/udev/rules.d/pci_powersave.conf
sudo mv disable_watchdog.conf /etc/sysctl.d/disable_watchdog.conf

# remove unnecessary packages
sudo apt purge epiphany-browser epiphany-browser-data
sudo apt purge midori-granite
sudo apt purge noise
sudo apt purge bluez
sudo apt purge modemmanager
sudo apt autoremove
sudo apt autoclean

# disable CTRL-V (paste) in Terminal
gsettings set org.pantheon.terminal.settings natural-copy-paste false
# increase terminal font
gsettings set org.pantheon.terminal.settings font 'DejaVu Sans Mono 15'
# background opaque transparency
gsettings set org.pantheon.terminal.settings background 'rgba(37, 46, 50, 1.0)'
# Set CTRL-SHIFT-T to duplicate tab
gsettings set org.pantheon.terminal.settings follow-last-tab true
