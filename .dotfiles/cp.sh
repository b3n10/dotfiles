#!/bin/bash
# makes transferring big files cpu-friendly on chromebook
#
# usage:
#
#   put this script into ~/Downloads/cp.sh and put this in your ~/.bashrc :
#
#     alias cp='bash ~/Downloads/cp.sh'
#
# from http://leon.vankammen.eu/2017/04/12/chromebook-survival-guide-for-linux-nodejs-developers.html
[[ ! -n $1 ]] && { /bin/cp; exit 0; }
FILESIZE=$(( $( stat -c '%s' "$1" ) / 1024 / 1024 ))
if [[ $FILESIZE -lt 50 ]]; then
	/bin/cp "$@"
else
	nice -n 20 ionice -c 3 /bin/cp "$@"
fi
