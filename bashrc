#!/bin/bash

export VISUAL=vim
export EDITOR="$VISUAL"
unset SSH_ASKPASS

alias vim=vi

PS1="[\e[0;32m\e[1m\u\e[m\e[0;33m\e[1m@\e[m\e[0;34m\e[1m\h\e[m \e[91m\e[1m\w\e[m]\n\\$ "

# set terminal title
trap 'printf "\033]0;%s\007" "${BASH_COMMAND//[^[:print:]]/}"' DEBUG
